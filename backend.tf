terraform {
  backend "gcs" {
    bucket  = "tabcorp_tfstates"
    prefix  = "dev"
  }
}